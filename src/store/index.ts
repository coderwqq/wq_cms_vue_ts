
import { createStore, Store, useStore as useVueStore } from 'vuex'
import login from './login/login'
import system from './main/system/system'
import dashboard from './main/analysis/dashboard'
import { getPageListDate } from '@/service/main/system/system'

import { IRootState } from './types'
import { IStoreType } from './types'

const store = createStore<IRootState>({
  state: () => {
    return {
      name: 'coderwhy',
      age: 123456,
      entireDepartment: [],
      entireRole: [],
      entireMenu: []
    }
  },
  mutations: {
    changeEntireDepartment(state, departmentList) {
      state.entireDepartment = departmentList
    },
    changeEntireRole(state, roleList) {
      state.entireRole = roleList
    },
    changeEntireMenu(state, menuList) {
      state.entireMenu = menuList
    }
  },
  getters: {},
  actions: {
    async getInitialDataAction({ commit }) {
      // 部门列表
      const departmentResult = await getPageListDate('/department/list', {
        offset: 0,
        size: 100
      })
      const { list: departmentList } = departmentResult.data
      commit('changeEntireDepartment', departmentList)

      // 角色列表
      const roleResult = await getPageListDate('/role/list', {
        offset: 0,
        size: 100
      })
      const { list: roleList } = roleResult.data
      commit('changeEntireRole', roleList)

      // 菜单列表
      const menuResult = await getPageListDate('/menu/list', {
        offset: 0,
        size: 100
      })
      const { list: menuList } = menuResult.data
      commit('changeEntireMenu', menuList)

    }
  },
  modules: {
    login,
    system,
    dashboard
  }
})

export function setupStore() {
  store.dispatch('login/loadLocalLogin')
  // store.dispatch('getInitialDataAction')
}

export function useStore(): Store<IStoreType> {
  return useVueStore()
}

export default store
