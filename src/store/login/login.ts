import { Module } from "vuex"
import {
  accountLoginRequest,
  userInfoRequest,
  userMenuInfoRequest
} from '@/service/login/login'

import { IRootState } from "../types"
import { ILoginState } from "./types"
import { IAccount } from "@/service/login/types"
import localCache from '@/utils/cache'
import router from '@/router'
import { mapMenuToRoutes, mapMenuToPermission } from '@/utils/map-menus'

const loginModel:Module<ILoginState, IRootState> = {
  namespaced: true,
  state() {
    return {
      token: '',
      userInfo: {},
      userMenuInfo: [],
      permission: []
    }
  },
  getters: {},
  mutations: {
    changeToken(state, token: string) {
      state.token = token
    },
    changeUserInfo(state, userInfo: any) {
      state.userInfo = userInfo
    },
    changeUserMenuInfo(state, userMenuInfo: any) {
      state.userMenuInfo = userMenuInfo

      const routes = mapMenuToRoutes(userMenuInfo)

      routes.forEach((route) => {
        router.addRoute('main', route)
      })

      // 获取用户按钮的权限
      const permission = mapMenuToPermission(userMenuInfo)
      state.permission = permission
    }
  },
  actions: {
    async accountLoginAction({ commit, dispatch }, payload: IAccount) {
      // 1.实现登录逻辑
      const accountResult = await accountLoginRequest(payload)
      const { id, token } = accountResult.data
      commit('changeToken', token)
      localCache.setCache('token', token)

      // 发送初始化的请求(department/role列表)
      dispatch('getInitialDataAction', null, { root: true })

      // 2.请求用户信息
      const userInfoResult = await userInfoRequest(id)
      const userInfo = userInfoResult.data
      commit('changeUserInfo', userInfo)
      localCache.setCache('userInfo', userInfo)

      // 3.请求用户菜单
      const userMenuInfoResult = await userMenuInfoRequest(userInfo.role.id)
      const userMenuInfo = userMenuInfoResult.data
      commit('changeUserMenuInfo', userMenuInfo)
      localCache.setCache('userMenuInfo', userMenuInfo)

      // 4.跳到首页
      router.push('/main')

    },
    loadLocalLogin({ commit, dispatch }) {
      const token = localCache.getCache('token')
      if(token) {
        commit('changeToken', token)
      }
      // 发送初始化的请求(department/role列表)
      dispatch('getInitialDataAction', null, { root: true })

      const userInfo = localCache.getCache('userInfo')
      if(userInfo) {
        commit('changeUserInfo', userInfo)
      }
      const userMenuInfo = localCache.getCache('userMenuInfo')
      if(userMenuInfo) {
        commit('changeUserMenuInfo', userMenuInfo)
      }
    }

    // phoneLoginAction({ commit }, payload: any) {
    //   console.log('执行phoneLoginAction')
    // }
  }
}

export default loginModel
