
export interface ILoginState {
  token: string,
  userInfo: any,
  userMenuInfo: any,
  permission: string[]
}
