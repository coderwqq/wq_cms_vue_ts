
import { Module } from "vuex";
import { IDashboardState } from './types'
import { IRootState } from '@/store/types'
import {
  getCategoryCountData,
  getCategorySaleData,
  getCategoryFavorData,
  getAddressSaleData
} from '@/service/main/analysis/dashboard'

const dashboardModule: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state() {
    return {
      categoryGoodsCount: [],
      categoryGoodsSale: [],
      categoryGoodsFavor: [],
      addressGoodsSale: []
    }
  },
  getters: {},
  mutations: {
    changeCategoryCount(state, list) {
      state.categoryGoodsCount = list
    },
    changeCategorySale(state, list) {
      state.categoryGoodsSale = list
    },
    changeCategoryFavor(state, list) {
      state.categoryGoodsFavor = list
    },
    changeAddressSale(state, list) {
      state.addressGoodsSale = list
    },
  },
  actions: {
    async getDashboardDataAction({ commit }) {
      const categoryCountResult = await getCategoryCountData()
      commit('changeCategoryCount', categoryCountResult.data)

      const categorySaleResult = await getCategorySaleData()
      commit('changeCategorySale', categorySaleResult.data)

      const categoryFavorResult = await getCategoryFavorData()
      commit('changeCategoryFavor', categoryFavorResult.data)

      const addressSaleResult = await getAddressSaleData()
      commit('changeAddressSale', addressSaleResult.data)
    }
  }
}

export default dashboardModule
