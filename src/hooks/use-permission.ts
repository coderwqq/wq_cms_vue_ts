
import { useStore } from "@/store";

export function usePermission(pageName: string, handleName: string) {
  const store = useStore()
  const permissions = store.state.login.permission
  const verifyPermission = `system:${pageName}:${handleName}`
  // 返回布尔类型!!
  return !!permissions.find((item) => item === verifyPermission)
}
