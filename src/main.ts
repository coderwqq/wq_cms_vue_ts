import { createApp } from 'vue'
import 'normalize.css'
import './assets/css/index.less'
import { globalRegister } from '@/global'

// 全局引入
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import zhCn from 'element-plus/es/locale/lang/zh-cn'

import App from './App.vue'

import store from './store'
import router from '@/router'
import { setupStore } from './store'


const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.use(ElementPlus, {
  locale: zhCn,
})

app.use(globalRegister)
app.use(ElementPlus)
app.use(store)
setupStore()
app.use(router)

app.mount('#app')
