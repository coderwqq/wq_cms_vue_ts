
import { RouteRecordRaw } from 'vue-router'
import { IBreadcrumb } from '@/base-ui/breadcrumb'

let firstMenu: any = null

export function mapMenuToRoutes(useMenus: any[]): RouteRecordRaw[] {
  const routes: RouteRecordRaw[] = []

  // 1.先去加载所有的routes
  const allRoutes: RouteRecordRaw[] = []
  const routeFiles = require.context('@/router/main', true, /\.ts/)
  routeFiles.keys().forEach((key) => {
    const route = require('@/router/main' + key.split('.')[1])

    allRoutes.push(route.default)
  })

  // 2.根据菜单获取需要添加的routes
  const _recurseGetRoute = (menus: any[]) => {
    for(const menu of menus) {
      if(menu.type === 2) {
        const route = allRoutes.find((route) => route.path === menu.url)
        if(route) routes.push(route)
        if(!firstMenu) {
          firstMenu = menu
        }
      } else {
        _recurseGetRoute(menu.children)
      }
    }
  }
  _recurseGetRoute(useMenus)

  return routes
}
export { firstMenu }


export function pathMapToBreadcrumb(userMenus: any[], currentPath: string): any {
  const breadcrumbs: IBreadcrumb[] = []
  pathMapToMenu(userMenus, currentPath, breadcrumbs)
  return breadcrumbs
}

export function pathMapToMenu(userMenus: any[], currentPath: string, breadcrumbs?: IBreadcrumb[]): any {
  for(const menu of userMenus) {
    if(menu.type === 1) {
      const findMenu = pathMapToMenu(menu.children ?? [], currentPath)
      if(findMenu) {
        breadcrumbs?.push({ name: menu.name })
        breadcrumbs?.push({ name: findMenu.name })
        return findMenu
      }
    } else if(menu.type === 2 && menu.url === currentPath) {
      return menu
    }
  }
}

// export function pathMapToBreadcrumb(userMenus: any[], currentPath: string): any {
//   const breadcrumbs: IBreadcrumb[] = []

//   for(const menu of userMenus) {
//     if(menu.type === 1) {
//       const findMenu = pathMapToBreadcrumb(menu.children ?? [], currentPath)
//       if(findMenu) {
//         breadcrumbs.push({ name: menu.name })
//         breadcrumbs.push({ name: findMenu.name })
//         return findMenu
//       }
//     } else if(menu.type === 2 && menu.url === currentPath) {
//       return menu
//     }
//   }

//   return breadcrumbs
// }

// export function pathMapToMenu(userMenus: any[], currentPath: string): any {
//   for(const menu of userMenus) {
//     if(menu.type === 1) {
//       const findMenu = pathMapToMenu(menu.children ?? [], currentPath)
//       if(findMenu) {
//         return findMenu
//       }
//     } else if(menu.type === 2 && menu.url === currentPath) {
//       return menu
//     }
//   }
// }


// 映射按钮权限
export function mapMenuToPermission(useMenus: any[]) {
  const permission: string[] = []

  const _recurseGetPermission = (menus: any[]) => {
    for(const menu of menus) {
      if(menu.type === 1 || menu.type === 2) {
        _recurseGetPermission(menu.children ?? [])
      } else if(menu.type === 3) {
        permission.push(menu.permission)
      }
    }
  }
  _recurseGetPermission(useMenus)

  return permission
}

//
export function mapMenuToLeafKeys(menuList: any[]) {
  const leafKeys: number[] = []

  const _recurseGetLeafKeys = (menuList: any[]) => {
    for(const menu of menuList) {
      if(menu.children) {
        _recurseGetLeafKeys(menu.children)
      } else {
        leafKeys.push(menu.id)
      }
    }
  }
  _recurseGetLeafKeys(menuList)

  return leafKeys
}
