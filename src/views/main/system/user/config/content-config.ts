
export const contentTableConfig = {
  title: '用户列表',
  createName: '新建用户',
  showIndexColumn: true,
  showSelectColumn: true,
  handleSelection: (value: any) => {
    console.log(value)
  },
  propList: [
    { prop: 'name', label: '用户名', minWidth: '120' },
    { prop: 'realname', label: '真实姓名', minWidth: '120' },
    { prop: 'cellphone', label: '手机号码', minWidth: '120' },
    { prop: 'enable', label: '状态', minWidth: '120', slotName: 'status' },
    { prop: 'createAt', label: '创建时间', minWidth: '250', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', minWidth: '250', slotName: 'updateAt' },
    { label: '操作', minWidth: '160', slotName: 'handler' },
  ]
}
