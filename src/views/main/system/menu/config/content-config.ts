
export const contentTableConfig = {
  title: '菜单列表',
  createName: '新建菜单',
  showIndexColumn: false,
  showSelectColumn: false,
  handleSelection: (value: any) => {
    console.log(value)
  },
  propList: [
    { prop: 'name', label: '菜单名称', minWidth: '160' },
    { prop: 'type', label: '类型', minWidth: '60' },
    { prop: 'url', label: '菜单url', minWidth: '180' },
    { prop: 'icon', label: '菜单icon', minWidth: '100', slotName: 'status' },
    { prop: 'permission', label: '按钮权限', minWidth: '160' },
    { prop: 'createAt', label: '创建时间', minWidth: '180', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', minWidth: '180', slotName: 'updateAt' },
    { label: '操作', minWidth: '140', slotName: 'handler' },
  ],
  childrenProps: {
    rowKey: 'id',
    treeProps: {
      children: 'children'
    }
  },
  showFooter: false
}
