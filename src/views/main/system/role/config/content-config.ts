
export const contentTableConfig = {
  title: '角色列表',
  createName: '新建角色',
  showIndexColumn: true,
  showSelectColumn: true,
  handleSelection: (value: any) => {
    console.log(value)
  },
  propList: [
    { prop: 'name', label: '角色名', minWidth: '120' },
    { prop: 'intro', label: '权限介绍', minWidth: '120' },
    { prop: 'createAt', label: '创建时间', minWidth: '250', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', minWidth: '250', slotName: 'updateAt' },
    { label: '操作', minWidth: '160', slotName: 'handler' },
  ]
}
