
export const contentTableConfig = {
  title: '商品列表',
  createName: '新建商品',
  showIndexColumn: true,
  showSelectColumn: true,
  handleSelection: (value: any) => {
    console.log(value)
  },
  propList: [
    { prop: 'name', label: '商品名称', minWidth: '120' },
    { prop: 'oldPrice', label: '原价格', minWidth: '120', slotName: 'oldPrice' },
    { prop: 'newPrice', label: '现价格', minWidth: '120', slotName: 'newPrice' },
    { prop: 'imgUrl', label: '商品图片', minWidth: '120', slotName: 'image' },
    { prop: 'status', label: '状态', minWidth: '120', slotName: 'status' },
    { prop: 'createAt', label: '创建时间', minWidth: '250', slotName: 'createAt' },
    { prop: 'updateAt', label: '更新时间', minWidth: '250', slotName: 'updateAt' },
    { label: '操作', minWidth: '160', slotName: 'handler' },
  ]
}
