
export const rules = {
  name: [
    {
      required: true,
      message: '用户名为必传内容',
      trigger: 'blur'
    },
    {
      pattern: /^[A-z0-9]{5,12}$/,
      message: '用户名必须是5-12位字母或数字',
      trigger: 'blur'
    }
  ],
  password: [
    {
      required: true,
      message: '密码为必传内容',
      trigger: 'blur'
    },
    {
      pattern: /^[A-z0-9]{5,12}$/,
      message: '密码必须是5-12位字母或数字',
      trigger: 'blur'
    }
  ]
}
