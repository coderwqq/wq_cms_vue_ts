import PieEchart from './src/pie-echart.vue'
import RoleEchart from './src/role-echart.vue'
import LineEchart from './src/line-echart.vue'
import BarEchart from './src/bar-echart.vue'
import MapEchart from './src/map-echart.vue'

export { PieEchart, RoleEchart, LineEchart, BarEchart, MapEchart }
