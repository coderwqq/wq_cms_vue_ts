
export interface IAccount {
  name: string,
  password: number
}

export interface IAccountResult {
  id: number,
  name: string,
  token: string
}
