
import hyRequest from "@/service";
import { IAccount, IAccountResult } from "./types";
import { IDataType } from '../types'

enum LoginAPI {
  AccountLogin = '/login',
  UserInfo = '/users/',
  userMenuInfo = '/role/'  // role/id/menu
}

export function accountLoginRequest(account: IAccount) {
  return hyRequest.post<IDataType<IAccountResult>>({
    url: LoginAPI.AccountLogin,
    data: account
  })
}

export function userInfoRequest(id: number) {
  return hyRequest.get<IDataType>({
    url: LoginAPI.UserInfo + id
  })
}

export function userMenuInfoRequest(id: number) {
  return hyRequest.get<IDataType>({
    url: LoginAPI.userMenuInfo + id + '/menu'
  })
}
