
import HYRequest from "./request";
import { BASE_URL, BASE_TIME } from './request/config'
import localCache from '@/utils/cache'
// import { HYRequestConfig } from './request/types'

const hyRequest = new HYRequest({
  baseURL: BASE_URL,
  timeout: BASE_TIME,
  interceptors: {
    requestInterceptor: (config) => {
      const token = localCache.getCache('token')
      if(token) {
        if(config && config.headers) {
          config.headers.Authorization = `Bearer ${token}`
        }
      }

      return config
    },
    requestInterceptorCatch: (error) => {
      return error
    },
    responseInterceptor: (res) => {
      return res
    },
    responseInterceptorCatch: (error) => {
      return error
    }
  }

})

export default hyRequest
