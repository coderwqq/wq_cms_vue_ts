
import hyRequest from "@/service";
import { IDataType } from '@/service/types'

enum IDashboardAPI {
  getCategoryCount = '/goods/category/count',
  getCategorySale = '/goods/category/sale',
  getCategoryFavor = '/goods/category/favor',
  getAddressSale = '/goods/address/sale'
}

export function getCategoryCountData() {
  return hyRequest.get<IDataType>({
    url: IDashboardAPI.getCategoryCount
  })
}

export function getCategorySaleData() {
  return hyRequest.get<IDataType>({
    url: IDashboardAPI.getCategorySale
  })
}

export function getCategoryFavorData() {
  return hyRequest.get<IDataType>({
    url: IDashboardAPI.getCategoryFavor
  })
}

export function getAddressSaleData() {
  return hyRequest.get<IDataType>({
    url: IDashboardAPI.getAddressSale
  })
}
